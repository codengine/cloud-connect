import boto3


class DBUtil(object):
    
    def __init__(self):
        self.region_name = "ap-southeast-1"
        self.endpoint_url = "http://localhost:8000"
        self.db = boto3.client("dynamodb", region_name=self.region_name)
    
    def create_resource_table(self):
        table = self.db.create_table(
        TableName="AWSResource",
        KeySchema=[
            {
                "AttributeName": "study_name",
                "KeyType": "HASH"  #Partition key
            },
            {
                "AttributeName": "resource_name",
                "KeyType": "RANGE"  #Sort key
            }
        ],
        AttributeDefinitions=[
            # {
            #     "AttributeName": "bucket_name",
            #     "AttributeType": "S"
            # },
            {
                "AttributeName": "study_name",
                "AttributeType": "S"
            },
            {
                "AttributeName": "resource_name",
                "AttributeType": "S"
            },
            # {
            #     "AttributeName": "upload_status",
            #     "AttributeType": "N"
            # },
            # {
            #     "AttributeName": "study_id",
            #     "AttributeType": "S"
            # },
    
        ],
        ProvisionedThroughput={
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 10
        })
        
    def create_study_table(self):
        table = self.db.create_table(
        TableName="Study",
        KeySchema=[
            {
                "AttributeName": "study_name",
                "KeyType": "HASH"  #Partition key
            }
        ],
        AttributeDefinitions=[
            # {
            #     "AttributeName": "bucket_name",
            #     "AttributeType": "S"
            # },
            {
                "AttributeName": "study_name",
                "AttributeType": "S"
            },
            # {
            #     "AttributeName": "processed",
            #     "AttributeType": "N"
            # },
    
        ],
        ProvisionedThroughput={
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 10
        })
        
    def create_all_table(self):
        self.create_resource_table()
        self.create_study_table()
      
      
    def insert_study_if_not_exists(self, study_name):
        try:
            study_table = self.db.put_item(
                TableName='Study',
                    Item={
                        'study_name':{
                            "S": study_name
                        },
                        'processed':{
                            "N": "0"
                        },
                    },
                    ConditionExpression='attribute_not_exists(study_name)'
                )
            return True
        except Exception as exp:
            return False
        
        
    def insert_study_item(self, bucket_name, study_name, item_name, upload_status=0, study_id=None):
        try:
            item = {
                'bucket_name': {
                    "S": bucket_name
                },
                'study_name': {
                    "S": study_name
                },
                'resource_name': {
                    "S": item_name
                },
                'upload_status': {
                    "N": "%s" % upload_status
                }
            }
            if study_id:
                item["study_id"] = {
                    "S": study_id
                }
            response = self.db.put_item(
                    TableName="AWSResource",
                    Item=item
            )
            print("Item added.")
        except Exception as exp:
            print("Item add Failed.")
            
    def read_unprocessed_studies(self, bucket_name, upload_status):
        try:
            items = []
            dynamodb = boto3.resource('dynamodb', region_name=self.region_name, endpoint_url=self.endpoint_url)
            study_table = dynamodb.Table('Study')
            fe = Key('processed').eq(0)
            pe = "bucket_name, study_name, processed"
            ean = {}
            
            response = study_table.scan(
                FilterExpression=fe,
                ProjectionExpression=pe,
                ExpressionAttributeNames=ean
                )
            
            for item in response['Items']:
                items += [item]
                
            while 'LastEvaluatedKey' in response:
                response = study_table.scan(
                    ProjectionExpression=pe,
                    FilterExpression=fe,
                    ExpressionAttributeNames= ean,
                    ExclusiveStartKey=response['LastEvaluatedKey']
                    )
            
                for item in response['Items']:
                    items += [item]
            return items
        except Exception as exp:
            pass
        
    def read_study_items(self, study_name, upload_status=None):
        try:
            items = []
            dynamodb = boto3.resource('dynamodb', region_name=self.region_name, endpoint_url=self.endpoint_url)
            resource_table = dynamodb.Table('AWSResource')
            fe = Key('study_name').eq(study_name)
            pe = "bucket_name, study_name, processed"
            ean = {}
        except Exception as exp:
            pass
        
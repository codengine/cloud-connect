import json
import boto3
from dbutil import DBUtil
from api_handler import APIHandler
from aws_s3 import AWSS3
from utils import Util


def lambda_handler(event, context):
    db = DBUtil()
    s3 = AWSS3()
    bucket_name = "alemhealth-windows-upload-test-bucket"
    
    if "DB" in event:
        # Create the DB
        try:
            print("Creating DB...")
            db.create_all_table()
            # db.insert_study_item(bucket_name='test_bucket', study_name='test_study', item_name='test_item')
            print("DB Created.")
            return "Created"
        except Exception as exp:
            print(str(exp))
            return "Creation Failed"
    
    # TODO implement
    
    records = event["Records"]
    for record in records:
        bucket = record["s3"]["bucket"]["name"]
        s3_object = record["s3"]["object"]["key"]
        folder = Util.detect_if_folder(s3_object)
        study_name, resource_name = Util.extract_resource_name(s3_object)
        inserted = db.insert_study_if_not_exists(study_name)
        if not folder:
            db.insert_study_item(bucket_name=bucket, study_name=study_name, item_name=s3_object)

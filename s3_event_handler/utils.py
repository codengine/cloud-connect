class Util(object):
    
    @staticmethod
    def detect_if_folder(cls, resource_name):
        if "." in resource_name:
            ext = resource_name[resource_name.rindex(".") + 1:]
            all_chrs = [chr(i) for i in range(ord('A'), ord('Z') + 1)]
            all_chrs += [chr(i) for i in range(ord('a'), ord('z') + 1)]
            if list(set(ext) & set(all_chrs)) == list(set(ext)):
                return False
            return True
        return True
        
    @classmethod
    def detect_if_resource(cls, resource_name):
        resource_name_split = resource_name.split("/")
        resource_name_split = [rn for rn in resource_name_split if rn]
        
        is_folder = True
        if "." in resource_name:
            ext = resource_name[resource_name.rindex(".") + 1:]
            all_chrs = [chr(i) for i in range(ord('A'), ord('Z') + 1)]
            all_chrs += [chr(i) for i in range(ord('a'), ord('z') + 1)]
            if list(set(ext) & set(all_chrs)) == list(set(ext)):
                is_folder = False
            else:
                is_folder = True
        else:
            is_folder = True
        
        return len(resource_name_split) >= 3 and not is_folder
    
    @classmethod
    def extract_resource_name(cls, resource_name):
        facility_guid, study_name, resource_name = None, None, resource_name
        resource_name_split = resource_name.split("/")
        # resource_name_split = [rn for rn in resource_name_split if rn]
        if len(resource_name_split) >= 3:
            facility_guid = resource_name_split[0]
            study_name = resource_name_split[1]
        return facility_guid, study_name, resource_name
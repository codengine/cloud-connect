class Util(object):
    
    @classmethod
    def detect_if_folder(cls, resource_name):
        if "." in resource_name:
            ext = resource_name[resource_name.rindex(".") + 1:]
            all_chrs = [chr(i) for i in range(ord('A'), ord('Z') + 1)]
            all_chrs += [chr(i) for i in range(ord('a'), ord('z') + 1)]
            if list(set(ext) & set(all_chrs)) == list(set(ext)):
                return False
            return True
        return True
    
    @classmethod
    def extract_resource_name(cls, resource_name):
        study_name, resource_name = None, resource_name
        resource_name_split = resource_name.split("/")
        # resource_name_split = [rn for rn in resource_name_split if rn]
        if len(resource_name_split) >= 2:
            study_name = resource_name_split[0]
        return study_name, resource_name